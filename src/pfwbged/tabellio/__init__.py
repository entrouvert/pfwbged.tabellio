# -*- coding: utf-8 -*-
"""Init and utils."""

from zope.i18nmessageid import MessageFactory

_ = MessageFactory('pfwbged.tabellio')

def initialize(context):
    """Initializer called when used as a Zope 2 product."""
    pass


from .monkey import patchDocumentViewer
patchDocumentViewer()
