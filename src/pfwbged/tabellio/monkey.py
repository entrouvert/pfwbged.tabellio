from Acquisition import aq_parent

from persistent.dict import PersistentDict
from DateTime import DateTime
from zope.annotation.interfaces import IAnnotations

import collective.documentviewer.convert
import collective.documentviewer.settings
import collective.documentviewer.views
from Products.Five.browser.resource import DirectoryResource

import plone.namedfile.utils

from . import documents

# Monkeypatching of various collective.documentviewer parts

def patched_publishTraverse(self, request, name):
    # don't check permissions for tabellio thumbnails; this would otherwise
    # require the objects to have a stable UID and to be registered in the
    # catalog.
    if name.endswith('-noisrev-tabellio'):
        fi = DirectoryResource.publishTraverse(self, request, name)
        return fi
    return self._old_publishTraverse(request, name)

def patched_base_init(self, context):
    # for our transient files we cannot store documentviewer information in
    # the object annotations so we have to create them under varying names
    # in the DocumentsFolder object.
    if isinstance(context, documents.TabellioDmsFile):
        self.context = context
        tabellio_folder = aq_parent(aq_parent(context))
        annotations = IAnnotations(tabellio_folder)
        # store metadata in top tabellio folder
        self._metadata = annotations.get('collective.documentviewer-%s' % context.id, None)
        if self._metadata is None:
            self._metadata = PersistentDict()
            self._metadata['last_updated'] = DateTime('1901/01/01').ISO8601()
            self.storage_version = collective.documentviewer.settings.STORAGE_VERSION
            annotations['collective.documentviewer-%s' % context.id] = self._metadata
    else:
        self._old___init__(context)


def patchDocumentViewer():
    collective.documentviewer.convert.Converter.isIndexationEnabled = lambda x: True
    # avoid streaming files as the blob hack breaks this
    plone.namedfile.utils.filestream_iterator = None
